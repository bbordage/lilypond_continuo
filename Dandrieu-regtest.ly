\include "realisation.ly"

\pointAndClickOff
\paper {
  indent = 0
}

global = { \set Score.tempoWholesPerMinute = #(ly:make-moment 72 1) \override Score.SpacingSpanner #'base-shortest-duration = #(ly:make-moment 1 2) }

parfaitMusic = \relative c, { f1 c' c, f c' g' g, c }
parfaitChiffrages = \figuremode { <_>1 <_> <_> <_> <_> <_> <_> <_> }

sixteMusic = \relative c, { f1 a c f, c' e g c, }
sixteChiffrages = \figuremode { <_>1 <6> <_> <_> <_> <6> <_> <_> }

petitesixteMusic = \relative c, { f1 a g f c' c, f c' e d c g' g, c }
petitesixteChiffrages = \figuremode { <_>1 <6> <6 _-> <_> <_> <_> <_> <_> <6> <6> <_> <_> <_> <_> }

faussequinteMusic = \relative c { f e f g a f c f c b c d e c g c }
faussequinteChiffrages = \figuremode { <_>1 <5-> <_> <6 _-> <6> <_> <_> <_> <_> <5/> <_> <6> <6> <_> <_> <_> }

quinteetsixteMusic = \relative c, { f g a bes c d e f e d c d bes c c, f }
quinteetsixteChiffrages = \figuremode { <_>1 <6 _-> <6> <6 5> <_> <6-> <5-> <_> <6> <6> <_> <6-> <6 5> <_> <_> <_> }

tritonMusic = \relative c { f e d c bes a g f g a bes c d e f c d bes c f, }
tritonChiffrages = \figuremode { <_>1 <6> <6> <_> <4/> <6> <6 _-> <_> <6 _-> <6> <6 5> <_> <6-> <5-> <_> <_> <6-> <6 5> <_> <_> }

quarteMusic = \relative c, { f g a bes c c, f c' d e f g g, c }
quarteChiffrages = \figuremode { <_>1 <6 _-> <6> <6 5> <4> <3> <_> <_> <6> <6> <6 5> <4> <3> <_> }

secondeMusic = \relative c, { f1 f e f bes c c, f c' c b c f g g, c }
secondeChiffrages = \figuremode { <_>1 <4- 2> <5-> <_> <6 5> <4> <3> <_> <_> <2> <5/> <_> <6 5> <4> <3> <_> }

septiemeMusic = \relative c { f f e a d, g c, f f e f bes, c c, f }
septiemeChiffrages = \figuremode { <_>1 <6> <7 5-> <7> <7> <7 _-> <7-> <_> <4- 2> <5-> <_> <6 5> <4> <3> <_> }

neuviemeMusic = \relative c, { f1 f e f f c' c d d, a' a bes bes f' f, bes bes c c, f }
neuviemeChiffrages = \figuremode { <_>1 <4- 2> <5-> <9> <8> <4> <3> <9> <8> <4> <3> <9> <8> <4-> <3> <7> <6 5> <4> <3> <_> }

quarteconsonanteMusic = \relative c { f1 f f f e f f, c' a d g, c c c c, f }
quarteconsonanteChiffrages = \figuremode { <_>1 <6 4-> <3> <4- 2> <5-> <9> <8> <_> <6> <7> <7 _-> <7-> <6 4> <4> <3> <_> }

septiemeetneuviemeMusic = \relative c { f1 f e f f, f f g g a a bes bes c f, c' c, f }
septiemeetneuviemeChiffrages = \figuremode { <_>1 <4- 2> <5-> <9> <8> <6 4-> <3> <9 7 _-> <6 _-> <9- 7> <6> <9 7> <6 5> <7-> <_> <4> <3> <_> }

septiemeaugMusic = \relative c, { f1 f f f f f e f f g g a a bes bes c f, c' c, f }
septiemeaugChiffrages = \figuremode { <_>1 <7+ 4-> <_> <6 4-> <3> <4- 2> <5-> <9> <8> <9 7 _-> <6 _-> <9- 7> <6> <9 7> <6 5> <7-> <_> <4> <3> <_> }

septiemeaugetsixteminMusic = \relative c, { f1 f f c' c, f c' c c g' g, c }
septiemeaugetsixteminChiffrages = \figuremode { <_>1 <7+ 4-> <_> <4> <3> <_> <_-> <7+ 6-> <_-> <4> <_+> <_-> }

sixtemajetfaussequinteMusic = \relative c, { f1 g e f f f c f c' d b! c c c g c }
sixtemajetfaussequinteChiffrages = \figuremode { <_>1 <6 _-> <5-> <_> <7+ 4-> <_> <_> <_> <_-> <6+ 5-> <5/> <_-> <7+ 6-> <_-> <_+> <_-> }

septiemedimMusic = \relative c, { f1 g a bes c d e, f g a f g c d bes c c c c f, }
septiemedimChiffrages = \figuremode { <_>1 <6 _-> <6> <6 5> <6 4> <6-> <5-> <_> <6 _-> <9- 7> <_> <9 7 _-> <_> <6-> <6 5> <_> <7+> <4> <3> <_> }

reglemajeureMusic = \relative c, { f1 g a bes c d e f e d c bes a g f c' c, f }
reglemajeureChiffrages = \figuremode { <_>1 <6/> <6> <6 5> <_> <6> <5/> <_> <6> <6/+> <_> <4/> <6> <6/> <_> <4> <3> <_> }

\book {
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \parfaitMusic \parfaitChiffrages \bar "|." }
      \new Staff << \clef F \parfaitMusic \parfaitChiffrages >>
    >>
    \header {
      piece = "Table de l'accord parfait"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \sixteMusic \sixteChiffrages \bar "|." }
      \new Staff << \clef F \sixteMusic \sixteChiffrages >>
    >>
    \header {
      piece = "Table de la sixte simple"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \petitesixteMusic \petitesixteChiffrages \bar "|." }
      \new Staff << \clef F \petitesixteMusic \petitesixteChiffrages >>
    >>
    \header {
      piece = "Table de la petite sixte"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \faussequinteMusic \faussequinteChiffrages \bar "|." }
      \new Staff << \clef F \faussequinteMusic \faussequinteChiffrages >>
    >>
    \header {
      piece = "Table de la fausse quinte"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \quinteetsixteMusic \quinteetsixteChiffrages \bar "|." }
      \new Staff << \clef F \quinteetsixteMusic \quinteetsixteChiffrages >>
    >>
    \header {
      piece = "Table de la quinte et sixte"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \tritonMusic \tritonChiffrages \bar "|." }
      \new Staff << \clef F \tritonMusic \tritonChiffrages >>
    >>
    \header {
      piece = "Table du triton"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \quarteMusic \quarteChiffrages \bar "|." }
      \new Staff << \clef F \quarteMusic \quarteChiffrages >>
    >>
    \header {
      piece = "Table de la quarte"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \secondeMusic \secondeChiffrages \bar "|." }
      \new Staff << \clef F \secondeMusic \secondeChiffrages >>
    >>
    \header {
      piece = "Table de la seconde"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \septiemeMusic \septiemeChiffrages \bar "|." }
      \new Staff << \clef F \septiemeMusic \septiemeChiffrages >>
    >>
    \header {
      piece = "Table de la septième"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \neuviemeMusic \neuviemeChiffrages \bar "|." }
      \new Staff << \clef F \neuviemeMusic \neuviemeChiffrages >>
    >>
    \header {
      piece = "Table de la neuvième"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \quarteconsonanteMusic \quarteconsonanteChiffrages \bar "|." }
      \new Staff << \clef F \quarteconsonanteMusic \quarteconsonanteChiffrages >>
    >>
    \header {
      piece = "Table de la quarte consonante"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \septiemeetneuviemeMusic \septiemeetneuviemeChiffrages \bar "|." }
      \new Staff << \clef F \septiemeetneuviemeMusic \septiemeetneuviemeChiffrages >>
    >>
    \header {
      piece = "Table de la septième et neuvième"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \septiemeaugMusic \septiemeaugChiffrages \bar "|." }
      \new Staff << \clef F \septiemeaugMusic \septiemeaugChiffrages >>
    >>
    \header {
      piece = "Table de la septième superflue"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \septiemeaugetsixteminMusic \septiemeaugetsixteminChiffrages \bar "|." }
      \new Staff << \clef F \septiemeaugetsixteminMusic \septiemeaugetsixteminChiffrages >>
    >>
    \header {
      piece = "Table de la septième superflue jointe à la sixte mineure"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
  
  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \sixtemajetfaussequinteMusic \sixtemajetfaussequinteChiffrages \bar "|." }
      \new Staff << \clef F \sixtemajetfaussequinteMusic \sixtemajetfaussequinteChiffrages >>
    >>
    \header {
      piece = "Table de la sixte majeure jointe à la fausse quinte"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key c \major \realisation c \major \septiemedimMusic \septiemedimChiffrages \bar "|." }
      \new Staff << \clef F \septiemedimMusic \septiemedimChiffrages >>
    >>
    \header {
      piece = "Table de la septième diminuée"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }

  \score {
    \new PianoStaff <<
      \new Staff { \global \key f \major \realisation f \major \reglemajeureMusic \reglemajeureChiffrages \bar "|." }
      \new Staff << \key f \major \clef F \reglemajeureMusic \reglemajeureChiffrages >>
    >>
    \header {
      piece = "Règle de l'octave dans une gamme majeure"
    }
    \layout {
      \context {
      \Score
      timing = ##f
      }
    }
    \midi {}
  }
}