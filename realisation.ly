#(define (extract-notes music)
         (let ((elements (ly:music-property music 'elements))
               (element (ly:music-property music 'element))
               (dur (ly:music-property music 'duration))
               (pitch (ly:music-property music 'pitch)))

              (cond
                ((ly:pitch? pitch)
                 (list dur pitch))

                ((pair? elements)
                 (map
                   (lambda (x) (extract-notes x))
                   elements))

                ((ly:music? element)
                 (extract-notes element)))))

#(define (extract-figures music)
         (let ((elements (ly:music-property music 'elements))
               (element (ly:music-property music 'element))
               (dur (ly:music-property music 'duration))
               (figure (ly:music-property music 'figure))
               (alter (ly:music-property music 'alteration))
               (dim (ly:music-property music 'diminished))
               (aug (ly:music-property music 'augmented)))

              (cond
                ((ly:duration? dur)
                 (list
                   dur
                   (if (null? figure) 3 figure)
                   alter
                   dim
                   aug))

              ((pair? elements)
               (map
                  (lambda (x) (extract-figures x))
                  elements))

              ((ly:music? element)
               (extract-figures element)))))

#(define interval-alist '((dim-second . (2 -1))
                          (min-second . (2 -1/2))
                          (second . 2)
                          (maj-second . (2 0))
                          (aug-second . (2 1/2))
                          (dim-third . (3 -1))
                          (min-third . (3 -1/2))
                          (third . 3)
                          (maj-third . (3 0))
                          (aug-third . (3 1/2))
                          (dim-fourth . (4 -1/2))
                          (fourth . 4)
                          (perfect-fourth . (4 0))
                          (aug-fourth . (4 1/2))
                          (dim-fifth . (5 -1/2))
                          (fifth . 5)
                          (perfect-fifth . (5 0))
                          (aug-fifth . (5 1/2))
                          (dim-sixth . (6 -1))
                          (min-sixth . (6 -1/2))
                          (sixth . 6)
                          (maj-sixth . (6 0))
                          (aug-sixth . (6 1/2))
                          (dim-seventh . (7 -1))
                          (min-seventh . (7 -1/2))
                          (seventh . 7)
                          (maj-seventh . (7 0))
                          (aug-seventh . (7 1/2))
                          (dim-octave . (8 -1/2))
                          (octave . 8)
                          (perfect-octave . (8 0))
                          (aug-octave . (8 1/2))
                          (min-ninth . (9 -1/2))
                          (ninth . (9 0))))

#(define (notename-transpose pitch interval)
         (modulo (+ (ly:pitch-notename pitch) (- interval 1)) 7))

#(define (transpose pitch abs-interval)
         (let* ((interval (first abs-interval))
                (alter (second abs-interval))
                (transposed-pitch (ly:pitch-transpose pitch (ly:make-pitch 0 (- interval 1) alter))))
               (ly:make-pitch 0 (ly:pitch-notename transposed-pitch) (ly:pitch-alteration transposed-pitch))))

#(define (transpose-abs-alter pitch interval alter)
         (ly:make-pitch 0 (notename-transpose pitch interval) alter))

#(define (transpose-to-scale scale pitch interval alter)
         (let* ((notename (notename-transpose pitch interval))
                (inscale-alter (cdr
                  (find
                    (lambda (x)
                            (= (car x)
                               notename))
                    scale))))
               (ly:make-pitch
                 0
                 notename
                 (+ inscale-alter
                    alter))))

#(define (smart-transpose scale pitch figure)
         (let* ((interval (second figure))
                (alter (third figure))
                (dim (fourth figure))
                (aug (fifth figure))
                (exceptions (exceptions interval alter dim)))
               (if (not exceptions)
                   (if (null? alter)
                           (transpose-to-scale scale pitch interval 0)
                       (if (zero? alter)
                           (transpose-abs-alter pitch interval alter)
                           (transpose-to-scale scale pitch interval alter)))
                   (transpose
                     pitch
                     (assq-ref interval-alist
                               exceptions)))))

#(define (exceptions interval alter dim)
           (cond
             ((not (null? alter))
              (case alter
                ((1/2) (case interval
                   ((2) 'aug-second)
                   ((3) 'maj-third)
                   ((4) 'aug-fourth)
                   ((5) 'aug-fifth)
                   ((6) 'maj-sixth)
                   ((7) 'maj-seventh)
                   (else #f)))
                ((-1/2) (case interval
                   ((2) 'min-second)
                   ((3) 'min-third)
                   ((5) 'dim-fifth)
                   ((6) 'min-sixth)
                   ((9) 'min-ninth)
                   (else #f)))
                (else #f)))
             ((and (= interval 8) (null? alter)) 'perfect-octave)
             ((not (null? dim)) (case interval ((4) 'aug-fourth)
                                                   ((5) 'dim-fifth)
                                                   ((6) 'maj-sixth)))
                 (else #f)))

#(define (make-figures dur interval-list)
         (map (lambda (interval)
                      (if (list? interval)
                          (if (number? (second interval))
                              (list dur (first interval) (second interval) '() '())
                              (list dur (first interval) '() (second interval) '()))
                          (list dur interval '() '() '())))
              interval-list))

#(define completion-table-dandrieu 
  '(
    ((2) (4 6))
    ((2 1/2) ((4 1/2) 6))
    ((3) (5))
    ((4) (5))
    ((4 1/2) (2 6)
            (((3 -1/2) (6))))
    ((5 -1/2) (3 6))
    ((5) (3))
    ((5 1/2) (3 7 9))
    ((6) (3)
       (((4 1/2) (3)) ((5) (3))))
    ((6 #t) (3 4))
    ((6 0) ()
           (((5 -1/2) (3))))
    ((7 -1) ((3 -1/2) (5 -1/2)))
    ((7) (3 5 (8 0))
       (((5 -1/2) (3))))
    ((7 0) (2 4 5)
           (((6 -1/2) (2 4))))
    ((8) (3 5))
    ((9) (3 5)
       (((7) (3 5))))
  )
)

#(define (find-figure-table completion-table figure)
         (find
           (lambda (x) (equal? (car x) figure))
           completion-table))

#(define (smart-find-figure-table completion-table figure)
         (let ((figure-table (find-figure-table completion-table figure)))
              (if (not figure-table)
                  (if (and (list? figure) (> (length figure) 1))
                      (if (= (length figure) 3)
                          (or (find-figure-table completion-table
                                (delete (second figure) figure))
                              (smart-find-figure-table completion-table (list-head figure 2)))
                          (smart-find-figure-table completion-table
                            (list-head figure 1))
                          )
                      '())
                  figure-table)))

#(define (browse-and-complete-figures completion-table sim-figures)
         (let* ((figure (car sim-figures))
                (figure-table (smart-find-figure-table completion-table figure)))
               (if (and (not (null? figure-table)) (= (length sim-figures) 1))
                   (second figure-table)
                   (if (> (length figure-table) 2)
                       (let ((result
                               (browse-and-complete-figures
                                 (third figure-table)
                                 (cdr sim-figures))))
                             (if (and (or (null? result) (not result)) (> (length figure) 1))
                                 (browse-and-complete-figures
                                   completion-table
                                   (list
                                     (list
                                       (first figure))
                                     (cadr sim-figures)))
                                 result))
                       '()))))

#(define (complete-figures sim-figures scale pitch)
         (let ((dur (caar sim-figures))
                (figure-list (map (lambda (figure)
                                          (remove null?
                                            (list
                                              (second figure)
                                              (ly:pitch-alteration
                                                (ly:pitch-diff
                                                  (smart-transpose
                                                    scale
                                                    pitch
                                                    figure)
                                                  pitch))
                                              (fourth figure))))
                                  sim-figures)))
               (if (< (length sim-figures) 3)
                   (append! sim-figures
                            (make-figures
                              dur
                              (browse-and-complete-figures
                                completion-table-dandrieu
                                figure-list))))
               
               (case (length sim-figures)
                     ((1) (append sim-figures (make-figures dur '(3 (8 0)))))
                     ((2) (append sim-figures (make-figures dur '((8 0)))))
                     (else sim-figures))))

#(define (calc-chord scale pitch sim-figures)
         (map
           (lambda (figure)
                   (smart-transpose scale pitch figure))
           (complete-figures sim-figures scale pitch)))

#(define (pitch-harmonic-quartertones pitch)
         (modulo (ly:pitch-quartertones pitch) 24))

#(define (same-note pitch-list)
         (null?
           (remove
             (lambda (x)
                     (= x (pitch-harmonic-quartertones (first pitch-list))))
             (map (lambda (pitch)
                          (pitch-harmonic-quartertones pitch))
                  pitch-list))))

#(define (pitch-distance pitch base-pitch)
         (abs
           (ly:pitch-quartertones
             (ly:pitch-diff
               pitch
               base-pitch))))

#(define (closest-pitch base-pitch pitch-list)
         (reduce
           (lambda
             (x y)
             (if
               (<
                 (pitch-distance x base-pitch)
                 (pitch-distance y base-pitch))
               x
               y))
           '()
           pitch-list))

#(define (pitch<? first-pitch second-pitch)
         (< (ly:pitch-quartertones first-pitch) (ly:pitch-quartertones second-pitch)))

#(define (transpose-to-closest base-pitch pitch-list)
         (let ((range-min (ly:pitch-transpose base-pitch (ly:make-pitch -1 4 0)))
               (range-max (ly:pitch-transpose base-pitch (ly:make-pitch 0 3 1/2))))
              (map
                (lambda
                  (pitch)
                  (let loop ((pitch pitch))
                            (cond
                              ((pitch<? pitch range-min)
                               (loop (ly:pitch-transpose pitch (ly:make-pitch 1 0 0))))
                              ((pitch<? range-max pitch)
                               (loop (ly:pitch-transpose pitch (ly:make-pitch -1 0 0))))
                              (else pitch))))
                pitch-list)))

#(define (voicing realisation-list)
         (let ((find-pitch
               (lambda (x y array-list)
               (let* ((array (reduce
                 (lambda (array previous-array)
                         (let* ((base-pitch (list-ref (first y) array))
                               (previous-base-pitch (list-ref (first y) previous-array))
                               (pitch (closest-pitch
                                        base-pitch
                                        (transpose-to-closest
                                          base-pitch
                                          x)))
                               (previous-pitch (closest-pitch
                                        previous-base-pitch
                                        (transpose-to-closest
                                          previous-base-pitch
                                          x))))
                              (if (< (pitch-distance pitch base-pitch)
                                     (pitch-distance previous-pitch previous-base-pitch))
                                  array
                                  previous-array)))
                 '()
                 array-list))
                     (base-pitch (list-ref (first y) array)))
                    (list array (closest-pitch
                                        base-pitch
                                        (transpose-to-closest
                                          base-pitch
                                          x)))))))
         (reverse
           (fold
             (lambda (x y)
                     (let* ((a (find-pitch x y (iota 3)))
                            (x-a (remove (lambda (z) (same-note (list (second a) z))) x))
                            (b (find-pitch x-a y (delete (first a) (iota 3))))
                            (x-ab (remove (lambda (z) (same-note (list (second b) z))) x-a))
                            (c (find-pitch x-ab y (delete (first b) (delete (first a) (iota 3))))))
                               (cons (list (second a) (second b) (second c)) y)))
             (list (car realisation-list))
             (list-tail realisation-list 1)))))

realisation = #(define-music-function (parser location tonic mode music figures)
                                      (ly:music? list? ly:music? ly:music?)
                                      (let* ((scale
                                              (ly:transpose-key-alist
                                                mode
                                                (ly:music-property
                                                  (car
                                                    (ly:music-property tonic 'elements))
                                                  'pitch)))
                                            (extracted-figures (extract-figures figures))
                                            (extracted-music (extract-notes music))
                                            (realisation-list (voicing (map (lambda (bass-note sim-figures)
                                                        (calc-chord scale (second (car bass-note)) sim-figures))
                                                extracted-music extracted-figures))))
                                           (make-simultaneous-music
                                             (map
                                               (lambda (voice-number)
                                                       (make-sequential-music
                                                         (map (lambda (bass-note chord)
                                                                      (let ((dur (first (car bass-note))))
                                                                            (if (> (length chord) voice-number)
                                                                                (make-music
                                                                                  'NoteEvent
                                                                                  'duration dur
                                                                                  'pitch
                                                                                    (list-ref chord voice-number))
                                                                                (make-skip-music dur))))
                                                              extracted-music realisation-list)))
                                               (iota 4)))))