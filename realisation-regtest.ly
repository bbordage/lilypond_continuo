\version "2.13"
\include "realisation.ly"

musicOne = \relative c {
  f1 f f f f f f f f f f f f f f f f f f f f f f
}

figuresOne = \figuremode {
  <_>1 <2> <2+> <3> <4> <4/> <4/ 3-> <5/> <5> <5+> <6> <6 4/> <6 5> <6/> <6+ 5-> <7--> <7-> <7- 5-> <7+> <7+ 6-> <8> <9> <9 7>
}

\score {
  <<
    \new Staff \with { instrumentName = "Baseline" } \relative c' { \key f \major <c f a>1 <d g bes> <d gis b> <c f a> <c f bes> <d g b> <d aes' b> <ces d a'> <c f a> <cis e g a> <d f a> <d a' b> <c d a'> <d a' bes> <ces d a'> <ces eeses aes> <c ees f a> <ces ees a> <c e g bes> <des e g bes> <c f a> <c g' a> <c e g a> }
    \new Staff \with { instrumentName = "Output" } { \key f \major \realisation f \major \musicOne \figuresOne }
    \new Staff \with { instrumentName = "Input" } << \key f \major \clef F \musicOne \figuresOne >>
  >>
}

musicTwo = \relative c {
  e1 e e bes bes bes f' f f b
}

figuresTwo = \figuremode {
  <5/>1 <5-> <5> <4/> <4+> <4> <7!> <7+> <7> <6->
}

\score {
  <<
    \new Staff \with { instrumentName = "Baseline" } \relative c' { \key f \major <c g' bes>1 q q <c e g> q q <c e g bes> q q }
    \new Staff \with { instrumentName = "Output" } { \key f \major \realisation f \major \musicTwo \figuresTwo }
    \new Staff \with { instrumentName = "Input" } << \key f \major \clef F \musicTwo \figuresTwo >>
  >>
}